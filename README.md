# UpeU Taller

## Instancias de Computación Elástica, Tipos, Imágenes, Balanceador de Carga

- 30 de junio 2021

- 01 de julio 2021

# Tema I

_Cloud Computing_

Generalidades y repaso de conocimientos adquiridos

[Presentacion](https://slides.com/pilasguru/01-upeu-cloud)

# Tema II

_Amazon Web Services (AWS) - Presentación_

Ejemplo de despliegue de una aplicación de Red Social

[Presentación](https://slides.com/pilasguru/02-upeu-aws)

# Tema III

_Amazon Web Services - Consola AWS_

Acercamiento practico al despliegue de EC2 (Instancia + Balanceador)

[Presentación](https://slides.com/pilasguru/03-upeu-aws)

# Tema IV

_Amazon Web Services - IaC_

Equivalente despliegue que en el ejemplo Tema III pero utilizando IaC

[Presentación](https://slides.com/pilasguru/04-upeu-aws)

---

![](./img/UPEU.jpg)
