#!/bin/bash -xe
amazon-linux-extras install -y docker=latest
systemctl enable --now docker.service
curl -s https://raw.githubusercontent.com/docker/docker-ce/master/components/cli/contrib/completion/bash/docker -o /etc/bash_completion.d/docker.sh
usermod -a -G docker ec2-user
docker pull alexwhen/docker-2048
docker pull oguzpastirmaci/hostname
docker run --detach -p 8080:80 --restart unless-stopped --name 2048 alexwhen/docker-2048
docker run --detach -p 80:8000 --restart unless-stopped --name hostname oguzpastirmaci/hostname
